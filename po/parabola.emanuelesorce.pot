# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the parabola.emanuelesorce package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: parabola.emanuelesorce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-17 05:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/About.qml:16 ../qml/About.qml:63 ../qml/Main.qml:64
#: parabola.desktop.in.h:1
msgid "Parabola"
msgstr ""

#: ../qml/About.qml:71
msgid ""
"Parabola is an open source app that pipes microphone input into audio output "
"turning your device into a microphone."
msgstr ""

#: ../qml/About.qml:79
msgid ""
"This app is licensed with GNU GPLv3 and uses some Suru icons in its logo"
msgstr ""

#: ../qml/About.qml:87
msgid "Feedback or any help is welcome, feel free to contact me!"
msgstr ""

#: ../qml/About.qml:92
msgid "❤Donate❤"
msgstr ""

#: ../qml/About.qml:100
msgid "Fork me on GitLab"
msgstr ""

#: ../qml/About.qml:107
msgid "Report bug or feature request"
msgstr ""

#: ../qml/About.qml:115
msgid "See License (GNU GPL v3)"
msgstr ""

#: ../qml/Main.qml:100
msgid "Microphone input is getting forwarded to audio output"
msgstr ""

#: ../qml/Main.qml:100
msgid "Screen won't turn off when this app is running"
msgstr ""

#: ../qml/Main.qml:108
msgid "AUDIO FRAMESKIP!"
msgstr ""

#: ../qml/Settings.qml:16
msgid "Settings"
msgstr ""

#: ../qml/Settings.qml:62
msgid "Preferred sample size:"
msgstr ""

#: ../qml/Settings.qml:65
msgid "8 bits - low"
msgstr ""

#: ../qml/Settings.qml:66
msgid "16 bits - high"
msgstr ""

#: ../qml/Settings.qml:67
msgid "24 bits - professional"
msgstr ""

#: ../qml/Settings.qml:68
msgid "32 bits - extreme"
msgstr ""

#: ../qml/Settings.qml:80
msgid "Preferred sample rate:"
msgstr ""

#: ../qml/Settings.qml:83
msgid "8000 Hz - normal"
msgstr ""

#: ../qml/Settings.qml:84
msgid "16000 Hz - high"
msgstr ""

#: ../qml/Settings.qml:85
msgid "32000 Hz - very high"
msgstr ""

#: ../qml/Settings.qml:86
msgid "44100 Hz - professional"
msgstr ""

#: ../qml/Settings.qml:99
msgid "Preferred number of channels:"
msgstr ""

#: ../qml/Settings.qml:102
msgid "Mono"
msgstr ""

#: ../qml/Settings.qml:103
msgid "Stereo"
msgstr ""
