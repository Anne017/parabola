/*
 * Copyright (C) 2021  Emanuele Sorce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * parabolic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINH
#define MAINH

#include <QGuiApplication>
#include <QCoreApplication>
#include <QUrl>
#include <QString>
#include <QQuickView>
#include <QQmlContext>

#include <QtConcurrent>

#include <QObject>
#include <QAudioInput>
#include <QAudioOutput>
#include <QBuffer>

// Class with operations accessible from C++
class micAPI : public QObject
{
	Q_OBJECT
	
	// input and output buffer
	QBuffer rdBuff;
	QBuffer wrBuff;

	// input and output streams
	QAudioInput *audioInput;
	QAudioOutput *audioOutput;
	
	// audio format used
	QAudioFormat format;
	int sample_rate;
	int sample_size;
	int channel_count;
	
	// if frameskip is used
	bool m_lagging;
	
	void audiothread();
	
	// used internally
	int audio_remainder;
	
public:
	
	Q_PROPERTY(bool lagging READ lagging WRITE setLagging NOTIFY laggingChanged)
	
	void setLagging(const bool& lagging);
	bool lagging();
	
	// start to pipe
	Q_INVOKABLE void start();
	
	// initialize everything
	Q_INVOKABLE bool init();
	
	// constructor
	// STILL REQUIRES init() to use this class!
	micAPI();
	
	// settings. Must be set before init()
	Q_INVOKABLE void setSampleSize(int ss);
	Q_INVOKABLE void setSampleRate(int sr);
	Q_INVOKABLE void setChannelCount(int cc);
	
	signals:
		void laggingChanged();
};

#endif
