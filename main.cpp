/*
 * Copyright (C) 2021  Emanuele Sorce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * parabolic is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"

// How much delay is tolerated?
#define TARGET_MAX_DELAY 190

micAPI::micAPI():
	// most compatible in theory
	sample_rate(8000),
	sample_size(8),
	channel_count(1),
	m_lagging(false)
{}

void micAPI::setSampleSize(int ss) {
	sample_size = ss;
}
void micAPI::setSampleRate(int sr) {
	sample_rate = sr;
}
void micAPI::setChannelCount(int cc) {
	channel_count = cc;
}
bool micAPI::lagging(){
	return m_lagging;
}
void micAPI::setLagging(const bool& lagging) {
	if(lagging == m_lagging) return;
	
	m_lagging = lagging;
	emit laggingChanged();
}

void micAPI::start() {
	audioOutput->start(&rdBuff);
	audioInput->start(&wrBuff);
}

void micAPI::audiothread() {
	
	// remove all data that was already read
	rdBuff.buffer().remove(0, rdBuff.pos());
	
	// set pointer to the beginning of the unread data
	const auto res = rdBuff.seek(0);
	assert(res);
	
	// If rdBuff is lagging behind a fraction of seconds, discard the buffer to ensure in and out stay in sync
	if(rdBuff.size() > format.bytesForDuration(TARGET_MAX_DELAY)) {
		rdBuff.buffer().clear();
		if(m_lagging == false)
			setLagging(true);
	}
	else if(m_lagging == true)
		setLagging(false);
	
	// write new data
	rdBuff.buffer().append(wrBuff.buffer());
	
	// remove all data that was already written
	wrBuff.buffer().clear();
	wrBuff.seek(0);
}

bool micAPI::init() {
	// Thanks for the tip https://stackoverflow.com/questions/40840142/how-to-implement-simple-audio-loopback-in-qt
	wrBuff.open(QBuffer::WriteOnly);
	rdBuff.open(QBuffer::ReadOnly);
	
	audio_remainder = 0;
	QObject::connect(&wrBuff, &QIODevice::bytesWritten, [this](qint64 bytes)
    {
		audio_remainder += bytes;
		if(audio_remainder > format.bytesForDuration(TARGET_MAX_DELAY * 0.3)) {
			audiothread();
			audio_remainder = 0;
		}
	});
	
	QAudioDeviceInfo inDevInfo = QAudioDeviceInfo::defaultInputDevice();
	QAudioDeviceInfo outDevInfo = QAudioDeviceInfo::defaultOutputDevice();
	
	format.setSampleRate(sample_rate);
	format.setSampleSize(sample_size);
	format.setChannelCount(channel_count);
	format.setCodec("audio/pcm");
	format.setByteOrder(QAudioFormat::LittleEndian);
	format.setSampleType(QAudioFormat::SignedInt);
	
	if(!inDevInfo.isFormatSupported(format) || !outDevInfo.isFormatSupported(format)){
		qDebug() << "Raw audio format not supported by backend, playing nearest configuration available.\n";
		format = inDevInfo.nearestFormat(format);
		format = outDevInfo.nearestFormat(format);
	}
	
	qDebug() << "Preferred sample rate: " << sample_rate;
	qDebug() << "Preferred sample size: " << sample_size;
	qDebug() << "Preferred channel count: " << channel_count;
	qDebug() << "Sample rate used: " << format.sampleRate();
	qDebug() << "Sample size used: " << format.sampleSize();
	qDebug() << "Channel count used: " << format.channelCount();
	
	audioInput = new QAudioInput(format);
	audioOutput = new QAudioOutput(format);
	
	return true;
}

int main(int argc, char *argv[])
{
	micAPI api;
	
	QGuiApplication *app = new QGuiApplication(argc, (char**)argv);
	app->setApplicationName("parabola.emanuelesorce");
	
	QQuickView *view = new QQuickView();
	
	// API
	view->rootContext()->setContextProperty("API", &api);
	
	view->setSource(QUrl("qrc:/Main.qml"));
	view->setResizeMode(QQuickView::SizeRootObjectToView);
	view->show();

	return app->exec();
}
